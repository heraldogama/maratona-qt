#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
   QApplication app {argc, argv};

   QApplication::setOrganizationName(QStringLiteral("mycompany"));
   QApplication::setOrganizationDomain(QStringLiteral("mycompany.com"));
   QApplication::setApplicationName(QStringLiteral("settings"));

   QApplication::setWindowIcon(QIcon {QStringLiteral(":/icons/qtlogo.svg")});
   MainWindow w;
   w.show();

   return QApplication::exec();
}
