var current = [
{
    "coord": {
        "lon": -38.51,
        "lat": -12.97
    },
    "weather": [
        {
            "id": 802,
            "main": "Clouds",
            "description": "scattered clouds",
            "icon": "03d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 27,
        "feels_like": 26.28,
        "temp_min": 27,
        "temp_max": 27,
        "pressure": 1019,
        "humidity": 65
    },
    "visibility": 10000,
    "wind": {
        "speed": 6.2,
        "deg": 110
    },
    "clouds": {
        "all": 40
    },
    "dt": 1600268350,
    "sys": {
        "type": 1,
        "id": 8454,
        "country": "BR",
        "sunrise": 1600244860,
        "sunset": 1600288201
    },
    "timezone": -10800,
    "id": 3450554,
    "name": "Salvador",
    "cod": 200
},
{
    "coord": {
        "lon": -46.64,
        "lat": -23.55
    },
    "weather": [
        {
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 28.35,
        "feels_like": 26.17,
        "temp_min": 27,
        "temp_max": 29.44,
        "pressure": 1019,
        "humidity": 37
    },
    "visibility": 10000,
    "wind": {
        "speed": 4.1,
        "deg": 330
    },
    "clouds": {
        "all": 14
    },
    "dt": 1600272371,
    "sys": {
        "type": 1,
        "id": 8394,
        "country": "BR",
        "sunrise": 1600246917,
        "sunset": 1600290046
    },
    "timezone": -10800,
    "id": 3448439,
    "name": "S\u00e3o Paulo",
    "cod": 200
},
{
    "coord": {
        "lon": -48.5,
        "lat": -1.46
    },
    "weather": [
        {
            "id": 802,
            "main": "Clouds",
            "description": "scattered clouds",
            "icon": "03d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 32,
        "feels_like": 34.7,
        "temp_min": 32,
        "temp_max": 32,
        "pressure": 1011,
        "humidity": 59
    },
    "visibility": 10000,
    "wind": {
        "speed": 3.6,
        "deg": 110
    },
    "clouds": {
        "all": 40
    },
    "dt": 1600272456,
    "sys": {
        "type": 1,
        "id": 8321,
        "country": "BR",
        "sunrise": 1600247143,
        "sunset": 1600290712
    },
    "timezone": -10800,
    "id": 3405870,
    "name": "Bel\u00e9m",
    "cod": 200
},
{
    "coord": {
        "lon": -40.84,
        "lat": -14.87
    },
    "weather": [
        {
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 26.98,
        "feels_like": 24.53,
        "temp_min": 26.98,
        "temp_max": 26.98,
        "pressure": 1016,
        "humidity": 37,
        "sea_level": 1016,
        "grnd_level": 918
    },
    "visibility": 10000,
    "wind": {
        "speed": 3.98,
        "deg": 100
    },
    "clouds": {
        "all": 18
    },
    "dt": 1600272685,
    "sys": {
        "country": "BR",
        "sunrise": 1600245438,
        "sunset": 1600288742
    },
    "timezone": -10800,
    "id": 3444914,
    "name": "Vit\u00f3ria da Conquista",
    "cod": 200
}
]

var sevenDays = [
{
    "lat": -12.97,
    "lon": -38.51,
    "timezone": "America/Bahia",
    "timezone_offset": -10800,
    "current": {
        "dt": 1600269014,
        "sunrise": 1600244860,
        "sunset": 1600288201,
        "temp": 27,
        "feels_like": 26.28,
        "pressure": 1019,
        "humidity": 65,
        "dew_point": 19.86,
        "uvi": 12.36,
        "clouds": 40,
        "visibility": 10000,
        "wind_speed": 6.2,
        "wind_deg": 110,
        "weather": [
            {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03d"
            }
        ]
    },
    "minutely": [
        {
            "dt": 1600269060,
            "precipitation": 0
        },
        {
            "dt": 1600269120,
            "precipitation": 0
        },
        {
            "dt": 1600269180,
            "precipitation": 0
        },
        {
            "dt": 1600269240,
            "precipitation": 0
        },
        {
            "dt": 1600269300,
            "precipitation": 0
        },
        {
            "dt": 1600269360,
            "precipitation": 0
        },
        {
            "dt": 1600269420,
            "precipitation": 0
        },
        {
            "dt": 1600269480,
            "precipitation": 0
        },
        {
            "dt": 1600269540,
            "precipitation": 0
        },
        {
            "dt": 1600269600,
            "precipitation": 0
        },
        {
            "dt": 1600269660,
            "precipitation": 0
        },
        {
            "dt": 1600269720,
            "precipitation": 0
        },
        {
            "dt": 1600269780,
            "precipitation": 0
        },
        {
            "dt": 1600269840,
            "precipitation": 0
        },
        {
            "dt": 1600269900,
            "precipitation": 0
        },
        {
            "dt": 1600269960,
            "precipitation": 0
        },
        {
            "dt": 1600270020,
            "precipitation": 0
        },
        {
            "dt": 1600270080,
            "precipitation": 0
        },
        {
            "dt": 1600270140,
            "precipitation": 0
        },
        {
            "dt": 1600270200,
            "precipitation": 0
        },
        {
            "dt": 1600270260,
            "precipitation": 0
        },
        {
            "dt": 1600270320,
            "precipitation": 0
        },
        {
            "dt": 1600270380,
            "precipitation": 0
        },
        {
            "dt": 1600270440,
            "precipitation": 0
        },
        {
            "dt": 1600270500,
            "precipitation": 0
        },
        {
            "dt": 1600270560,
            "precipitation": 0
        },
        {
            "dt": 1600270620,
            "precipitation": 0
        },
        {
            "dt": 1600270680,
            "precipitation": 0
        },
        {
            "dt": 1600270740,
            "precipitation": 0
        },
        {
            "dt": 1600270800,
            "precipitation": 0
        },
        {
            "dt": 1600270860,
            "precipitation": 0
        },
        {
            "dt": 1600270920,
            "precipitation": 0
        },
        {
            "dt": 1600270980,
            "precipitation": 0
        },
        {
            "dt": 1600271040,
            "precipitation": 0
        },
        {
            "dt": 1600271100,
            "precipitation": 0
        },
        {
            "dt": 1600271160,
            "precipitation": 0
        },
        {
            "dt": 1600271220,
            "precipitation": 0
        },
        {
            "dt": 1600271280,
            "precipitation": 0
        },
        {
            "dt": 1600271340,
            "precipitation": 0
        },
        {
            "dt": 1600271400,
            "precipitation": 0
        },
        {
            "dt": 1600271460,
            "precipitation": 0
        },
        {
            "dt": 1600271520,
            "precipitation": 0
        },
        {
            "dt": 1600271580,
            "precipitation": 0
        },
        {
            "dt": 1600271640,
            "precipitation": 0
        },
        {
            "dt": 1600271700,
            "precipitation": 0
        },
        {
            "dt": 1600271760,
            "precipitation": 0
        },
        {
            "dt": 1600271820,
            "precipitation": 0
        },
        {
            "dt": 1600271880,
            "precipitation": 0
        },
        {
            "dt": 1600271940,
            "precipitation": 0
        },
        {
            "dt": 1600272000,
            "precipitation": 0
        },
        {
            "dt": 1600272060,
            "precipitation": 0
        },
        {
            "dt": 1600272120,
            "precipitation": 0
        },
        {
            "dt": 1600272180,
            "precipitation": 0
        },
        {
            "dt": 1600272240,
            "precipitation": 0
        },
        {
            "dt": 1600272300,
            "precipitation": 0
        },
        {
            "dt": 1600272360,
            "precipitation": 0
        },
        {
            "dt": 1600272420,
            "precipitation": 0
        },
        {
            "dt": 1600272480,
            "precipitation": 0
        },
        {
            "dt": 1600272540,
            "precipitation": 0
        },
        {
            "dt": 1600272600,
            "precipitation": 0
        },
        {
            "dt": 1600272660,
            "precipitation": 0
        }
    ],
    "hourly": [
        {
            "dt": 1600268400,
            "temp": 27,
            "feels_like": 26.07,
            "pressure": 1019,
            "humidity": 65,
            "dew_point": 19.86,
            "clouds": 40,
            "visibility": 10000,
            "wind_speed": 6.51,
            "wind_deg": 120,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.38
        },
        {
            "dt": 1600272000,
            "temp": 25.98,
            "feels_like": 24.94,
            "pressure": 1018,
            "humidity": 68,
            "dew_point": 19.62,
            "clouds": 21,
            "visibility": 10000,
            "wind_speed": 6.5,
            "wind_deg": 121,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.33
        },
        {
            "dt": 1600275600,
            "temp": 25.32,
            "feels_like": 24.1,
            "pressure": 1017,
            "humidity": 69,
            "dew_point": 19.22,
            "clouds": 9,
            "visibility": 10000,
            "wind_speed": 6.5,
            "wind_deg": 122,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.33
        },
        {
            "dt": 1600279200,
            "temp": 25.02,
            "feels_like": 23.81,
            "pressure": 1016,
            "humidity": 69,
            "dew_point": 18.94,
            "clouds": 3,
            "visibility": 10000,
            "wind_speed": 6.3,
            "wind_deg": 118,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.25
        },
        {
            "dt": 1600282800,
            "temp": 24.74,
            "feels_like": 23.76,
            "pressure": 1016,
            "humidity": 70,
            "dew_point": 18.9,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 5.95,
            "wind_deg": 119,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600286400,
            "temp": 24.5,
            "feels_like": 23.65,
            "pressure": 1016,
            "humidity": 70,
            "dew_point": 18.89,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 5.62,
            "wind_deg": 116,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600290000,
            "temp": 24.04,
            "feels_like": 23.37,
            "pressure": 1016,
            "humidity": 72,
            "dew_point": 18.85,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 5.37,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600293600,
            "temp": 23.95,
            "feels_like": 23.49,
            "pressure": 1017,
            "humidity": 73,
            "dew_point": 19,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 5.15,
            "wind_deg": 114,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600297200,
            "temp": 23.77,
            "feels_like": 23.34,
            "pressure": 1017,
            "humidity": 75,
            "dew_point": 19.2,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 5.28,
            "wind_deg": 119,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600300800,
            "temp": 23.8,
            "feels_like": 23.04,
            "pressure": 1018,
            "humidity": 76,
            "dew_point": 19.41,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 5.9,
            "wind_deg": 123,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600304400,
            "temp": 23.72,
            "feels_like": 22.89,
            "pressure": 1019,
            "humidity": 76,
            "dew_point": 19.36,
            "clouds": 1,
            "visibility": 10000,
            "wind_speed": 5.95,
            "wind_deg": 124,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.12
        },
        {
            "dt": 1600308000,
            "temp": 23.7,
            "feels_like": 23.02,
            "pressure": 1018,
            "humidity": 75,
            "dew_point": 19.21,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 5.59,
            "wind_deg": 123,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.16
        },
        {
            "dt": 1600311600,
            "temp": 23.57,
            "feels_like": 23.11,
            "pressure": 1018,
            "humidity": 75,
            "dew_point": 18.94,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 5.19,
            "wind_deg": 127,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1600315200,
            "temp": 23.46,
            "feels_like": 23.04,
            "pressure": 1017,
            "humidity": 74,
            "dew_point": 18.62,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 4.94,
            "wind_deg": 132,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1600318800,
            "temp": 23.43,
            "feels_like": 22.82,
            "pressure": 1016,
            "humidity": 71,
            "dew_point": 17.99,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 4.78,
            "wind_deg": 138,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600322400,
            "temp": 23.39,
            "feels_like": 22.66,
            "pressure": 1016,
            "humidity": 68,
            "dew_point": 17.24,
            "clouds": 1,
            "visibility": 10000,
            "wind_speed": 4.52,
            "wind_deg": 137,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600326000,
            "temp": 23.44,
            "feels_like": 22.47,
            "pressure": 1016,
            "humidity": 64,
            "dew_point": 16.45,
            "clouds": 92,
            "visibility": 10000,
            "wind_speed": 4.35,
            "wind_deg": 141,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600329600,
            "temp": 23.49,
            "feels_like": 22.43,
            "pressure": 1017,
            "humidity": 62,
            "dew_point": 15.93,
            "clouds": 96,
            "visibility": 10000,
            "wind_speed": 4.24,
            "wind_deg": 148,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600333200,
            "temp": 23.54,
            "feels_like": 22.51,
            "pressure": 1018,
            "humidity": 60,
            "dew_point": 15.43,
            "clouds": 84,
            "visibility": 10000,
            "wind_speed": 3.95,
            "wind_deg": 159,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600336800,
            "temp": 23.94,
            "feels_like": 23.02,
            "pressure": 1018,
            "humidity": 59,
            "dew_point": 15.48,
            "clouds": 65,
            "visibility": 10000,
            "wind_speed": 3.84,
            "wind_deg": 160,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600340400,
            "temp": 24.25,
            "feels_like": 23.43,
            "pressure": 1019,
            "humidity": 58,
            "dew_point": 15.53,
            "clouds": 54,
            "visibility": 10000,
            "wind_speed": 3.72,
            "wind_deg": 156,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600344000,
            "temp": 24.49,
            "feels_like": 23.91,
            "pressure": 1019,
            "humidity": 58,
            "dew_point": 15.8,
            "clouds": 49,
            "visibility": 10000,
            "wind_speed": 3.49,
            "wind_deg": 155,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600347600,
            "temp": 24.65,
            "feels_like": 24.07,
            "pressure": 1019,
            "humidity": 60,
            "dew_point": 16.49,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 3.87,
            "wind_deg": 158,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.19
        },
        {
            "dt": 1600351200,
            "temp": 24.68,
            "feels_like": 24.19,
            "pressure": 1018,
            "humidity": 63,
            "dew_point": 17.28,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 4.19,
            "wind_deg": 159,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.19
        },
        {
            "dt": 1600354800,
            "temp": 24.59,
            "feels_like": 24.18,
            "pressure": 1017,
            "humidity": 67,
            "dew_point": 18.25,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 4.61,
            "wind_deg": 157,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.36
        },
        {
            "dt": 1600358400,
            "temp": 24.37,
            "feels_like": 23.94,
            "pressure": 1015,
            "humidity": 72,
            "dew_point": 19.09,
            "clouds": 11,
            "visibility": 10000,
            "wind_speed": 5.22,
            "wind_deg": 154,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.39
        },
        {
            "dt": 1600362000,
            "temp": 24.28,
            "feels_like": 23.84,
            "pressure": 1014,
            "humidity": 75,
            "dew_point": 19.58,
            "clouds": 18,
            "visibility": 10000,
            "wind_speed": 5.61,
            "wind_deg": 151,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.4,
            "rain": {
                "1h": 0.18
            }
        },
        {
            "dt": 1600365600,
            "temp": 24.23,
            "feels_like": 23.96,
            "pressure": 1014,
            "humidity": 75,
            "dew_point": 19.53,
            "clouds": 20,
            "visibility": 10000,
            "wind_speed": 5.34,
            "wind_deg": 149,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.36,
            "rain": {
                "1h": 0.12
            }
        },
        {
            "dt": 1600369200,
            "temp": 24.29,
            "feels_like": 24.22,
            "pressure": 1014,
            "humidity": 74,
            "dew_point": 19.51,
            "clouds": 10,
            "visibility": 10000,
            "wind_speed": 4.94,
            "wind_deg": 145,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1600372800,
            "temp": 24.32,
            "feels_like": 24.48,
            "pressure": 1014,
            "humidity": 73,
            "dew_point": 19.23,
            "clouds": 6,
            "visibility": 10000,
            "wind_speed": 4.5,
            "wind_deg": 138,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1600376400,
            "temp": 24.28,
            "feels_like": 24.34,
            "pressure": 1015,
            "humidity": 71,
            "dew_point": 18.82,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 4.33,
            "wind_deg": 120,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600380000,
            "temp": 24.24,
            "feels_like": 23.9,
            "pressure": 1016,
            "humidity": 71,
            "dew_point": 18.76,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 4.88,
            "wind_deg": 111,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600383600,
            "temp": 24.05,
            "feels_like": 23.6,
            "pressure": 1016,
            "humidity": 71,
            "dew_point": 18.57,
            "clouds": 3,
            "visibility": 10000,
            "wind_speed": 4.92,
            "wind_deg": 112,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600387200,
            "temp": 24.1,
            "feels_like": 23.55,
            "pressure": 1016,
            "humidity": 69,
            "dew_point": 18.29,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 4.81,
            "wind_deg": 110,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600390800,
            "temp": 23.99,
            "feels_like": 23.46,
            "pressure": 1017,
            "humidity": 70,
            "dew_point": 18.3,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 4.85,
            "wind_deg": 105,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600394400,
            "temp": 23.77,
            "feels_like": 23.42,
            "pressure": 1017,
            "humidity": 72,
            "dew_point": 18.6,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 4.74,
            "wind_deg": 98,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600398000,
            "temp": 23.55,
            "feels_like": 23.53,
            "pressure": 1016,
            "humidity": 75,
            "dew_point": 18.93,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 4.55,
            "wind_deg": 88,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600401600,
            "temp": 23.33,
            "feels_like": 23.52,
            "pressure": 1016,
            "humidity": 76,
            "dew_point": 18.91,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 4.25,
            "wind_deg": 84,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600405200,
            "temp": 23.28,
            "feels_like": 23.55,
            "pressure": 1015,
            "humidity": 75,
            "dew_point": 18.75,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.98,
            "wind_deg": 88,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600408800,
            "temp": 23.26,
            "feels_like": 23.77,
            "pressure": 1015,
            "humidity": 76,
            "dew_point": 18.81,
            "clouds": 8,
            "visibility": 10000,
            "wind_speed": 3.75,
            "wind_deg": 94,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600412400,
            "temp": 23.16,
            "feels_like": 23.76,
            "pressure": 1015,
            "humidity": 76,
            "dew_point": 18.72,
            "clouds": 54,
            "visibility": 10000,
            "wind_speed": 3.57,
            "wind_deg": 98,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1600416000,
            "temp": 23,
            "feels_like": 23.65,
            "pressure": 1015,
            "humidity": 77,
            "dew_point": 18.83,
            "clouds": 77,
            "visibility": 10000,
            "wind_speed": 3.53,
            "wind_deg": 98,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1600419600,
            "temp": 23.16,
            "feels_like": 23.92,
            "pressure": 1016,
            "humidity": 77,
            "dew_point": 18.96,
            "clouds": 60,
            "visibility": 10000,
            "wind_speed": 3.47,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0.12
        },
        {
            "dt": 1600423200,
            "temp": 23.52,
            "feels_like": 24.64,
            "pressure": 1017,
            "humidity": 75,
            "dew_point": 18.89,
            "clouds": 47,
            "visibility": 10000,
            "wind_speed": 2.91,
            "wind_deg": 102,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.32,
            "rain": {
                "1h": 0.12
            }
        },
        {
            "dt": 1600426800,
            "temp": 23.85,
            "feels_like": 25.22,
            "pressure": 1018,
            "humidity": 73,
            "dew_point": 18.84,
            "clouds": 40,
            "visibility": 10000,
            "wind_speed": 2.48,
            "wind_deg": 105,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.37,
            "rain": {
                "1h": 0.13
            }
        },
        {
            "dt": 1600430400,
            "temp": 24.34,
            "feels_like": 25.76,
            "pressure": 1018,
            "humidity": 71,
            "dew_point": 18.83,
            "clouds": 38,
            "visibility": 10000,
            "wind_speed": 2.42,
            "wind_deg": 111,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.37,
            "rain": {
                "1h": 0.13
            }
        },
        {
            "dt": 1600434000,
            "temp": 24.74,
            "feels_like": 25.85,
            "pressure": 1017,
            "humidity": 69,
            "dew_point": 18.81,
            "clouds": 1,
            "visibility": 10000,
            "wind_speed": 2.81,
            "wind_deg": 116,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.25
        },
        {
            "dt": 1600437600,
            "temp": 24.92,
            "feels_like": 25.62,
            "pressure": 1016,
            "humidity": 70,
            "dew_point": 19.14,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 3.66,
            "wind_deg": 120,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.21
        }
    ],
    "daily": [
        {
            "dt": 1600264800,
            "sunrise": 1600244860,
            "sunset": 1600288201,
            "temp": {
                "day": 27,
                "min": 23.72,
                "max": 27,
                "night": 24,
                "eve": 24.68,
                "morn": 23.72
            },
            "feels_like": {
                "day": 26.07,
                "night": 23.33,
                "eve": 24.18,
                "morn": 22.82
            },
            "pressure": 1019,
            "humidity": 65,
            "dew_point": 19.86,
            "wind_speed": 6.51,
            "wind_deg": 120,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 40,
            "pop": 0.84,
            "rain": 5.91,
            "uvi": 12.36
        },
        {
            "dt": 1600351200,
            "sunrise": 1600331217,
            "sunset": 1600374600,
            "temp": {
                "day": 24.59,
                "min": 23.39,
                "max": 24.59,
                "night": 24.1,
                "eve": 24.28,
                "morn": 23.54
            },
            "feels_like": {
                "day": 24.18,
                "night": 23.55,
                "eve": 24.34,
                "morn": 22.51
            },
            "pressure": 1017,
            "humidity": 67,
            "dew_point": 18.25,
            "wind_speed": 4.61,
            "wind_deg": 157,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 5,
            "pop": 0.36,
            "rain": 0.36,
            "uvi": 12.29
        },
        {
            "dt": 1600437600,
            "sunrise": 1600417573,
            "sunset": 1600461000,
            "temp": {
                "day": 25.02,
                "min": 23.16,
                "max": 25.02,
                "night": 24.01,
                "eve": 24.13,
                "morn": 23.16
            },
            "feels_like": {
                "day": 25.24,
                "night": 23.42,
                "eve": 23.47,
                "morn": 23.92
            },
            "pressure": 1015,
            "humidity": 71,
            "dew_point": 19.51,
            "wind_speed": 4.56,
            "wind_deg": 123,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 5,
            "pop": 0.37,
            "rain": 0.64,
            "uvi": 11.84
        },
        {
            "dt": 1600524000,
            "sunrise": 1600503930,
            "sunset": 1600547399,
            "temp": {
                "day": 25,
                "min": 22.72,
                "max": 25.07,
                "night": 24.21,
                "eve": 24.6,
                "morn": 22.72
            },
            "feels_like": {
                "day": 25.67,
                "night": 23.97,
                "eve": 24.38,
                "morn": 23.67
            },
            "pressure": 1015,
            "humidity": 67,
            "dew_point": 18.47,
            "wind_speed": 3.31,
            "wind_deg": 150,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 6,
            "pop": 0.16,
            "uvi": 12.05
        },
        {
            "dt": 1600610400,
            "sunrise": 1600590286,
            "sunset": 1600633798,
            "temp": {
                "day": 25.49,
                "min": 23.06,
                "max": 25.55,
                "night": 24.7,
                "eve": 24.93,
                "morn": 23.06
            },
            "feels_like": {
                "day": 26.34,
                "night": 24.37,
                "eve": 24.65,
                "morn": 24.97
            },
            "pressure": 1015,
            "humidity": 77,
            "dew_point": 21.33,
            "wind_speed": 4.88,
            "wind_deg": 111,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 42,
            "pop": 0.45,
            "rain": 0.7,
            "uvi": 11.95
        },
        {
            "dt": 1600696800,
            "sunrise": 1600676642,
            "sunset": 1600720198,
            "temp": {
                "day": 25.78,
                "min": 24.2,
                "max": 25.83,
                "night": 24.82,
                "eve": 25.06,
                "morn": 24.2
            },
            "feels_like": {
                "day": 25.49,
                "night": 24.72,
                "eve": 24.37,
                "morn": 24.36
            },
            "pressure": 1016,
            "humidity": 79,
            "dew_point": 21.89,
            "wind_speed": 7.02,
            "wind_deg": 81,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 17,
            "pop": 1,
            "rain": 3.09,
            "uvi": 12.32
        },
        {
            "dt": 1600783200,
            "sunrise": 1600762999,
            "sunset": 1600806597,
            "temp": {
                "day": 25.84,
                "min": 24.05,
                "max": 25.84,
                "night": 24.44,
                "eve": 24.8,
                "morn": 24.25
            },
            "feels_like": {
                "day": 25.45,
                "night": 24.66,
                "eve": 25,
                "morn": 24.24
            },
            "pressure": 1018,
            "humidity": 74,
            "dew_point": 20.92,
            "wind_speed": 6.42,
            "wind_deg": 108,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 2,
            "pop": 0.64,
            "rain": 1.88,
            "uvi": 12.58
        },
        {
            "dt": 1600869600,
            "sunrise": 1600849356,
            "sunset": 1600892997,
            "temp": {
                "day": 25.56,
                "min": 23.77,
                "max": 25.56,
                "night": 24.23,
                "eve": 24.56,
                "morn": 23.77
            },
            "feels_like": {
                "day": 24.83,
                "night": 24.73,
                "eve": 24.43,
                "morn": 22.8
            },
            "pressure": 1018,
            "humidity": 73,
            "dew_point": 20.54,
            "wind_speed": 6.56,
            "wind_deg": 114,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 23,
            "pop": 0.82,
            "rain": 3.61,
            "uvi": 12.24
        }
    ]
},
{
    "lat": -23.56,
    "lon": -46.64,
    "timezone": "America/Sao_Paulo",
    "timezone_offset": -10800,
    "current": {
        "dt": 1600272609,
        "sunrise": 1600246917,
        "sunset": 1600290046,
        "temp": 28.2,
        "feels_like": 25.98,
        "pressure": 1019,
        "humidity": 37,
        "dew_point": 12.14,
        "uvi": 10.13,
        "clouds": 14,
        "visibility": 10000,
        "wind_speed": 4.1,
        "wind_deg": 330,
        "weather": [
            {
                "id": 801,
                "main": "Clouds",
                "description": "few clouds",
                "icon": "02d"
            }
        ]
    },
    "minutely": [
        {
            "dt": 1600272660,
            "precipitation": 0
        },
        {
            "dt": 1600272720,
            "precipitation": 0
        },
        {
            "dt": 1600272780,
            "precipitation": 0
        },
        {
            "dt": 1600272840,
            "precipitation": 0
        },
        {
            "dt": 1600272900,
            "precipitation": 0
        },
        {
            "dt": 1600272960,
            "precipitation": 0
        },
        {
            "dt": 1600273020,
            "precipitation": 0
        },
        {
            "dt": 1600273080,
            "precipitation": 0
        },
        {
            "dt": 1600273140,
            "precipitation": 0
        },
        {
            "dt": 1600273200,
            "precipitation": 0
        },
        {
            "dt": 1600273260,
            "precipitation": 0
        },
        {
            "dt": 1600273320,
            "precipitation": 0
        },
        {
            "dt": 1600273380,
            "precipitation": 0
        },
        {
            "dt": 1600273440,
            "precipitation": 0
        },
        {
            "dt": 1600273500,
            "precipitation": 0
        },
        {
            "dt": 1600273560,
            "precipitation": 0
        },
        {
            "dt": 1600273620,
            "precipitation": 0
        },
        {
            "dt": 1600273680,
            "precipitation": 0
        },
        {
            "dt": 1600273740,
            "precipitation": 0
        },
        {
            "dt": 1600273800,
            "precipitation": 0
        },
        {
            "dt": 1600273860,
            "precipitation": 0
        },
        {
            "dt": 1600273920,
            "precipitation": 0
        },
        {
            "dt": 1600273980,
            "precipitation": 0
        },
        {
            "dt": 1600274040,
            "precipitation": 0
        },
        {
            "dt": 1600274100,
            "precipitation": 0
        },
        {
            "dt": 1600274160,
            "precipitation": 0
        },
        {
            "dt": 1600274220,
            "precipitation": 0
        },
        {
            "dt": 1600274280,
            "precipitation": 0
        },
        {
            "dt": 1600274340,
            "precipitation": 0
        },
        {
            "dt": 1600274400,
            "precipitation": 0
        },
        {
            "dt": 1600274460,
            "precipitation": 0
        },
        {
            "dt": 1600274520,
            "precipitation": 0
        },
        {
            "dt": 1600274580,
            "precipitation": 0
        },
        {
            "dt": 1600274640,
            "precipitation": 0
        },
        {
            "dt": 1600274700,
            "precipitation": 0
        },
        {
            "dt": 1600274760,
            "precipitation": 0
        },
        {
            "dt": 1600274820,
            "precipitation": 0
        },
        {
            "dt": 1600274880,
            "precipitation": 0
        },
        {
            "dt": 1600274940,
            "precipitation": 0
        },
        {
            "dt": 1600275000,
            "precipitation": 0
        },
        {
            "dt": 1600275060,
            "precipitation": 0
        },
        {
            "dt": 1600275120,
            "precipitation": 0
        },
        {
            "dt": 1600275180,
            "precipitation": 0
        },
        {
            "dt": 1600275240,
            "precipitation": 0
        },
        {
            "dt": 1600275300,
            "precipitation": 0
        },
        {
            "dt": 1600275360,
            "precipitation": 0
        },
        {
            "dt": 1600275420,
            "precipitation": 0
        },
        {
            "dt": 1600275480,
            "precipitation": 0
        },
        {
            "dt": 1600275540,
            "precipitation": 0
        },
        {
            "dt": 1600275600,
            "precipitation": 0
        },
        {
            "dt": 1600275660,
            "precipitation": 0
        },
        {
            "dt": 1600275720,
            "precipitation": 0
        },
        {
            "dt": 1600275780,
            "precipitation": 0
        },
        {
            "dt": 1600275840,
            "precipitation": 0
        },
        {
            "dt": 1600275900,
            "precipitation": 0
        },
        {
            "dt": 1600275960,
            "precipitation": 0
        },
        {
            "dt": 1600276020,
            "precipitation": 0
        },
        {
            "dt": 1600276080,
            "precipitation": 0
        },
        {
            "dt": 1600276140,
            "precipitation": 0
        },
        {
            "dt": 1600276200,
            "precipitation": 0
        },
        {
            "dt": 1600276260,
            "precipitation": 0
        }
    ],
    "hourly": [
        {
            "dt": 1600272000,
            "temp": 28.2,
            "feels_like": 28.37,
            "pressure": 1019,
            "humidity": 37,
            "dew_point": 12.14,
            "clouds": 14,
            "visibility": 10000,
            "wind_speed": 0.69,
            "wind_deg": 324,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600275600,
            "temp": 29.49,
            "feels_like": 28.96,
            "pressure": 1017,
            "humidity": 31,
            "dew_point": 10.6,
            "clouds": 15,
            "visibility": 10000,
            "wind_speed": 1.04,
            "wind_deg": 308,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600279200,
            "temp": 29.55,
            "feels_like": 28.88,
            "pressure": 1015,
            "humidity": 29,
            "dew_point": 9.66,
            "clouds": 21,
            "visibility": 10000,
            "wind_speed": 0.87,
            "wind_deg": 315,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600282800,
            "temp": 28.15,
            "feels_like": 27.44,
            "pressure": 1014,
            "humidity": 33,
            "dew_point": 10.37,
            "clouds": 65,
            "visibility": 10000,
            "wind_speed": 1.21,
            "wind_deg": 155,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600286400,
            "temp": 25.28,
            "feels_like": 23.29,
            "pressure": 1014,
            "humidity": 49,
            "dew_point": 13.81,
            "clouds": 65,
            "visibility": 10000,
            "wind_speed": 4.54,
            "wind_deg": 151,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600290000,
            "temp": 23.03,
            "feels_like": 22.38,
            "pressure": 1014,
            "humidity": 62,
            "dew_point": 15.46,
            "clouds": 49,
            "visibility": 10000,
            "wind_speed": 3.42,
            "wind_deg": 141,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600293600,
            "temp": 22.72,
            "feels_like": 22.4,
            "pressure": 1014,
            "humidity": 64,
            "dew_point": 15.57,
            "clouds": 38,
            "visibility": 10000,
            "wind_speed": 3.05,
            "wind_deg": 133,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600297200,
            "temp": 22.03,
            "feels_like": 21.84,
            "pressure": 1015,
            "humidity": 66,
            "dew_point": 15.48,
            "clouds": 36,
            "visibility": 10000,
            "wind_speed": 2.78,
            "wind_deg": 122,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600300800,
            "temp": 21.84,
            "feels_like": 21.59,
            "pressure": 1016,
            "humidity": 65,
            "dew_point": 15.17,
            "clouds": 36,
            "visibility": 10000,
            "wind_speed": 2.65,
            "wind_deg": 124,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600304400,
            "temp": 21.76,
            "feels_like": 21.63,
            "pressure": 1016,
            "humidity": 65,
            "dew_point": 14.98,
            "clouds": 19,
            "visibility": 10000,
            "wind_speed": 2.43,
            "wind_deg": 117,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600308000,
            "temp": 21.5,
            "feels_like": 21.56,
            "pressure": 1016,
            "humidity": 65,
            "dew_point": 14.79,
            "clouds": 10,
            "visibility": 10000,
            "wind_speed": 2.03,
            "wind_deg": 103,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600311600,
            "temp": 21.2,
            "feels_like": 21.46,
            "pressure": 1015,
            "humidity": 66,
            "dew_point": 14.6,
            "clouds": 8,
            "visibility": 10000,
            "wind_speed": 1.73,
            "wind_deg": 103,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600315200,
            "temp": 20.91,
            "feels_like": 21.09,
            "pressure": 1015,
            "humidity": 66,
            "dew_point": 14.44,
            "clouds": 6,
            "visibility": 10000,
            "wind_speed": 1.71,
            "wind_deg": 85,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600318800,
            "temp": 20.46,
            "feels_like": 20.65,
            "pressure": 1014,
            "humidity": 68,
            "dew_point": 14.5,
            "clouds": 5,
            "visibility": 10000,
            "wind_speed": 1.7,
            "wind_deg": 62,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600322400,
            "temp": 20.2,
            "feels_like": 20.2,
            "pressure": 1014,
            "humidity": 69,
            "dew_point": 14.5,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 1.96,
            "wind_deg": 40,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600326000,
            "temp": 20,
            "feels_like": 19.94,
            "pressure": 1014,
            "humidity": 69,
            "dew_point": 14.3,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 1.96,
            "wind_deg": 18,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600329600,
            "temp": 20.05,
            "feels_like": 19.91,
            "pressure": 1014,
            "humidity": 66,
            "dew_point": 13.6,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 1.76,
            "wind_deg": 7,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600333200,
            "temp": 19.91,
            "feels_like": 19.3,
            "pressure": 1014,
            "humidity": 62,
            "dew_point": 12.53,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 1.93,
            "wind_deg": 7,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600336800,
            "temp": 20.64,
            "feels_like": 19.31,
            "pressure": 1014,
            "humidity": 54,
            "dew_point": 11.06,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 2.36,
            "wind_deg": 19,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600340400,
            "temp": 23.01,
            "feels_like": 20.85,
            "pressure": 1014,
            "humidity": 42,
            "dew_point": 9.75,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 2.92,
            "wind_deg": 15,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600344000,
            "temp": 26.2,
            "feels_like": 23.6,
            "pressure": 1015,
            "humidity": 33,
            "dew_point": 8.87,
            "clouds": 3,
            "visibility": 10000,
            "wind_speed": 3.28,
            "wind_deg": 4,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600347600,
            "temp": 28.84,
            "feels_like": 26.2,
            "pressure": 1014,
            "humidity": 28,
            "dew_point": 8.62,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.28,
            "wind_deg": 338,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600351200,
            "temp": 30.74,
            "feels_like": 27.8,
            "pressure": 1014,
            "humidity": 24,
            "dew_point": 8.29,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.48,
            "wind_deg": 325,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600354800,
            "temp": 32.02,
            "feels_like": 28.9,
            "pressure": 1013,
            "humidity": 21,
            "dew_point": 7.34,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.44,
            "wind_deg": 303,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600358400,
            "temp": 32.42,
            "feels_like": 29.04,
            "pressure": 1011,
            "humidity": 21,
            "dew_point": 7.42,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.92,
            "wind_deg": 293,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600362000,
            "temp": 31.52,
            "feels_like": 28.48,
            "pressure": 1011,
            "humidity": 22,
            "dew_point": 7.55,
            "clouds": 16,
            "visibility": 10000,
            "wind_speed": 3.41,
            "wind_deg": 286,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600365600,
            "temp": 30.43,
            "feels_like": 28.65,
            "pressure": 1010,
            "humidity": 25,
            "dew_point": 8.78,
            "clouds": 26,
            "visibility": 10000,
            "wind_speed": 1.93,
            "wind_deg": 248,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600369200,
            "temp": 27.3,
            "feels_like": 25.5,
            "pressure": 1010,
            "humidity": 40,
            "dew_point": 12.59,
            "clouds": 98,
            "visibility": 10000,
            "wind_speed": 3.68,
            "wind_deg": 168,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600372800,
            "temp": 23.62,
            "feels_like": 22.38,
            "pressure": 1011,
            "humidity": 63,
            "dew_point": 16.27,
            "clouds": 98,
            "visibility": 10000,
            "wind_speed": 4.69,
            "wind_deg": 144,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600376400,
            "temp": 22.45,
            "feels_like": 21.67,
            "pressure": 1011,
            "humidity": 68,
            "dew_point": 16.28,
            "clouds": 86,
            "visibility": 10000,
            "wind_speed": 4.08,
            "wind_deg": 152,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600380000,
            "temp": 21.03,
            "feels_like": 20.78,
            "pressure": 1012,
            "humidity": 73,
            "dew_point": 16.04,
            "clouds": 74,
            "visibility": 10000,
            "wind_speed": 3.2,
            "wind_deg": 132,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600383600,
            "temp": 21.21,
            "feels_like": 21.05,
            "pressure": 1013,
            "humidity": 70,
            "dew_point": 15.72,
            "clouds": 68,
            "visibility": 10000,
            "wind_speed": 2.81,
            "wind_deg": 141,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600387200,
            "temp": 20.7,
            "feels_like": 20.09,
            "pressure": 1013,
            "humidity": 72,
            "dew_point": 15.64,
            "clouds": 67,
            "visibility": 10000,
            "wind_speed": 3.42,
            "wind_deg": 150,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600390800,
            "temp": 20.04,
            "feels_like": 19.2,
            "pressure": 1014,
            "humidity": 74,
            "dew_point": 15.48,
            "clouds": 79,
            "visibility": 10000,
            "wind_speed": 3.64,
            "wind_deg": 158,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600394400,
            "temp": 20.24,
            "feels_like": 19.31,
            "pressure": 1014,
            "humidity": 71,
            "dew_point": 14.94,
            "clouds": 70,
            "visibility": 10000,
            "wind_speed": 3.54,
            "wind_deg": 161,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600398000,
            "temp": 19.87,
            "feels_like": 18.14,
            "pressure": 1014,
            "humidity": 73,
            "dew_point": 14.9,
            "clouds": 78,
            "visibility": 10000,
            "wind_speed": 4.72,
            "wind_deg": 156,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600401600,
            "temp": 19.11,
            "feels_like": 17.25,
            "pressure": 1014,
            "humidity": 77,
            "dew_point": 15.1,
            "clouds": 65,
            "visibility": 10000,
            "wind_speed": 4.96,
            "wind_deg": 158,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600405200,
            "temp": 18.8,
            "feels_like": 17.41,
            "pressure": 1013,
            "humidity": 77,
            "dew_point": 14.88,
            "clouds": 59,
            "visibility": 10000,
            "wind_speed": 4.13,
            "wind_deg": 126,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600408800,
            "temp": 18.47,
            "feels_like": 17.27,
            "pressure": 1013,
            "humidity": 80,
            "dew_point": 15.08,
            "clouds": 62,
            "visibility": 10000,
            "wind_speed": 4,
            "wind_deg": 133,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600412400,
            "temp": 18.26,
            "feels_like": 17.39,
            "pressure": 1013,
            "humidity": 81,
            "dew_point": 15.04,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 3.52,
            "wind_deg": 137,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600416000,
            "temp": 18.29,
            "feels_like": 17.35,
            "pressure": 1013,
            "humidity": 80,
            "dew_point": 14.8,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 3.53,
            "wind_deg": 130,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600419600,
            "temp": 18.28,
            "feels_like": 17.2,
            "pressure": 1013,
            "humidity": 79,
            "dew_point": 14.65,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 3.64,
            "wind_deg": 127,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600423200,
            "temp": 18.59,
            "feels_like": 17.93,
            "pressure": 1014,
            "humidity": 76,
            "dew_point": 14.4,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 2.88,
            "wind_deg": 111,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600426800,
            "temp": 19.12,
            "feels_like": 18.27,
            "pressure": 1014,
            "humidity": 72,
            "dew_point": 14.09,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 3,
            "wind_deg": 95,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600430400,
            "temp": 21.65,
            "feels_like": 20.27,
            "pressure": 1014,
            "humidity": 60,
            "dew_point": 13.62,
            "clouds": 98,
            "visibility": 10000,
            "wind_speed": 3.56,
            "wind_deg": 86,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600434000,
            "temp": 24.19,
            "feels_like": 22.92,
            "pressure": 1013,
            "humidity": 50,
            "dew_point": 13.37,
            "clouds": 65,
            "visibility": 10000,
            "wind_speed": 3.2,
            "wind_deg": 82,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600437600,
            "temp": 27.14,
            "feels_like": 26.27,
            "pressure": 1013,
            "humidity": 40,
            "dew_point": 12.53,
            "clouds": 46,
            "visibility": 10000,
            "wind_speed": 2.29,
            "wind_deg": 58,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600441200,
            "temp": 30.14,
            "feels_like": 29.11,
            "pressure": 1012,
            "humidity": 29,
            "dew_point": 10.51,
            "clouds": 63,
            "visibility": 10000,
            "wind_speed": 1.58,
            "wind_deg": 15,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        }
    ],
    "daily": [
        {
            "dt": 1600268400,
            "sunrise": 1600246917,
            "sunset": 1600290046,
            "temp": {
                "day": 28.2,
                "min": 16.73,
                "max": 29.04,
                "night": 22.25,
                "eve": 24.15,
                "morn": 16.73
            },
            "feels_like": {
                "day": 28.2,
                "night": 22.05,
                "eve": 23.4,
                "morn": 15.76
            },
            "pressure": 1019,
            "humidity": 37,
            "dew_point": 12.14,
            "wind_speed": 0.93,
            "wind_deg": 349,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "clouds": 14,
            "pop": 0.11,
            "uvi": 10.13
        },
        {
            "dt": 1600354800,
            "sunrise": 1600333254,
            "sunset": 1600376464,
            "temp": {
                "day": 32.02,
                "min": 19.91,
                "max": 32.02,
                "night": 20.7,
                "eve": 22.45,
                "morn": 19.91
            },
            "feels_like": {
                "day": 28.9,
                "night": 20.09,
                "eve": 21.67,
                "morn": 19.3
            },
            "pressure": 1013,
            "humidity": 21,
            "dew_point": 7.34,
            "wind_speed": 3.44,
            "wind_deg": 303,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 0,
            "pop": 0,
            "uvi": 10.56
        },
        {
            "dt": 1600441200,
            "sunrise": 1600419592,
            "sunset": 1600462883,
            "temp": {
                "day": 30.14,
                "min": 18.28,
                "max": 30.14,
                "night": 20.28,
                "eve": 21.9,
                "morn": 18.28
            },
            "feels_like": {
                "day": 29.11,
                "night": 20.5,
                "eve": 21.18,
                "morn": 17.2
            },
            "pressure": 1012,
            "humidity": 29,
            "dew_point": 10.51,
            "wind_speed": 1.58,
            "wind_deg": 15,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 63,
            "pop": 0.04,
            "uvi": 9.96
        },
        {
            "dt": 1600527600,
            "sunrise": 1600505929,
            "sunset": 1600549301,
            "temp": {
                "day": 31.51,
                "min": 19.39,
                "max": 31.51,
                "night": 20.04,
                "eve": 24.63,
                "morn": 19.39
            },
            "feels_like": {
                "day": 30.46,
                "night": 19.52,
                "eve": 22.93,
                "morn": 19.55
            },
            "pressure": 1011,
            "humidity": 28,
            "dew_point": 11.19,
            "wind_speed": 1.87,
            "wind_deg": 229,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 93,
            "pop": 0.99,
            "rain": 3.41,
            "uvi": 10.04
        },
        {
            "dt": 1600610400,
            "sunrise": 1600592266,
            "sunset": 1600635720,
            "temp": {
                "day": 14.77,
                "min": 13.37,
                "max": 19.02,
                "night": 13.39,
                "eve": 13.37,
                "morn": 17.31
            },
            "feels_like": {
                "day": 11.44,
                "night": 10.48,
                "eve": 9.87,
                "morn": 14.59
            },
            "pressure": 1020,
            "humidity": 82,
            "dew_point": 11.78,
            "wind_speed": 5.52,
            "wind_deg": 154,
            "weather": [
                {
                    "id": 502,
                    "main": "Rain",
                    "description": "heavy intensity rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 1,
            "rain": 19.87,
            "uvi": 9.72
        },
        {
            "dt": 1600696800,
            "sunrise": 1600678603,
            "sunset": 1600722138,
            "temp": {
                "day": 17.42,
                "min": 13.82,
                "max": 17.42,
                "night": 15.57,
                "eve": 15.82,
                "morn": 14.5
            },
            "feels_like": {
                "day": 15.77,
                "night": 13.78,
                "eve": 14.18,
                "morn": 12.38
            },
            "pressure": 1020,
            "humidity": 73,
            "dew_point": 12.66,
            "wind_speed": 3.47,
            "wind_deg": 137,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 97,
            "pop": 0.96,
            "rain": 9.45,
            "uvi": 10.47
        },
        {
            "dt": 1600783200,
            "sunrise": 1600764941,
            "sunset": 1600808557,
            "temp": {
                "day": 18.42,
                "min": 15.51,
                "max": 19.5,
                "night": 17.45,
                "eve": 18.94,
                "morn": 16.04
            },
            "feels_like": {
                "day": 18.11,
                "night": 17.05,
                "eve": 18.23,
                "morn": 14.98
            },
            "pressure": 1019,
            "humidity": 77,
            "dew_point": 14.44,
            "wind_speed": 2.4,
            "wind_deg": 82,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 0.56,
            "rain": 0.94,
            "uvi": 10.79
        },
        {
            "dt": 1600869600,
            "sunrise": 1600851278,
            "sunset": 1600894976,
            "temp": {
                "day": 26.6,
                "min": 16.38,
                "max": 26.6,
                "night": 20.23,
                "eve": 22.2,
                "morn": 16.38
            },
            "feels_like": {
                "day": 27.11,
                "night": 20.24,
                "eve": 21.95,
                "morn": 16.81
            },
            "pressure": 1019,
            "humidity": 46,
            "dew_point": 14.14,
            "wind_speed": 1.09,
            "wind_deg": 296,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 20,
            "pop": 0.6,
            "rain": 1.59,
            "uvi": 10.68
        }
    ]
},
{
    "lat": -1.46,
    "lon": -48.5,
    "timezone": "America/Belem",
    "timezone_offset": -10800,
    "current": {
        "dt": 1600272663,
        "sunrise": 1600247143,
        "sunset": 1600290712,
        "temp": 32,
        "feels_like": 34.7,
        "pressure": 1011,
        "humidity": 59,
        "dew_point": 22.98,
        "uvi": 13.22,
        "clouds": 40,
        "visibility": 10000,
        "wind_speed": 3.6,
        "wind_deg": 110,
        "weather": [
            {
                "id": 802,
                "main": "Clouds",
                "description": "scattered clouds",
                "icon": "03d"
            }
        ]
    },
    "minutely": [
        {
            "dt": 1600272720,
            "precipitation": 0
        },
        {
            "dt": 1600272780,
            "precipitation": 0
        },
        {
            "dt": 1600272840,
            "precipitation": 0
        },
        {
            "dt": 1600272900,
            "precipitation": 0
        },
        {
            "dt": 1600272960,
            "precipitation": 0
        },
        {
            "dt": 1600273020,
            "precipitation": 0
        },
        {
            "dt": 1600273080,
            "precipitation": 0
        },
        {
            "dt": 1600273140,
            "precipitation": 0
        },
        {
            "dt": 1600273200,
            "precipitation": 0
        },
        {
            "dt": 1600273260,
            "precipitation": 0
        },
        {
            "dt": 1600273320,
            "precipitation": 0
        },
        {
            "dt": 1600273380,
            "precipitation": 0
        },
        {
            "dt": 1600273440,
            "precipitation": 0
        },
        {
            "dt": 1600273500,
            "precipitation": 0
        },
        {
            "dt": 1600273560,
            "precipitation": 0
        },
        {
            "dt": 1600273620,
            "precipitation": 0
        },
        {
            "dt": 1600273680,
            "precipitation": 0
        },
        {
            "dt": 1600273740,
            "precipitation": 0
        },
        {
            "dt": 1600273800,
            "precipitation": 0
        },
        {
            "dt": 1600273860,
            "precipitation": 0
        },
        {
            "dt": 1600273920,
            "precipitation": 0
        },
        {
            "dt": 1600273980,
            "precipitation": 0
        },
        {
            "dt": 1600274040,
            "precipitation": 0
        },
        {
            "dt": 1600274100,
            "precipitation": 0
        },
        {
            "dt": 1600274160,
            "precipitation": 0
        },
        {
            "dt": 1600274220,
            "precipitation": 0
        },
        {
            "dt": 1600274280,
            "precipitation": 0
        },
        {
            "dt": 1600274340,
            "precipitation": 0
        },
        {
            "dt": 1600274400,
            "precipitation": 0
        },
        {
            "dt": 1600274460,
            "precipitation": 0
        },
        {
            "dt": 1600274520,
            "precipitation": 0
        },
        {
            "dt": 1600274580,
            "precipitation": 0
        },
        {
            "dt": 1600274640,
            "precipitation": 0
        },
        {
            "dt": 1600274700,
            "precipitation": 0
        },
        {
            "dt": 1600274760,
            "precipitation": 0
        },
        {
            "dt": 1600274820,
            "precipitation": 0
        },
        {
            "dt": 1600274880,
            "precipitation": 0
        },
        {
            "dt": 1600274940,
            "precipitation": 0
        },
        {
            "dt": 1600275000,
            "precipitation": 0
        },
        {
            "dt": 1600275060,
            "precipitation": 0
        },
        {
            "dt": 1600275120,
            "precipitation": 0
        },
        {
            "dt": 1600275180,
            "precipitation": 0
        },
        {
            "dt": 1600275240,
            "precipitation": 0
        },
        {
            "dt": 1600275300,
            "precipitation": 0
        },
        {
            "dt": 1600275360,
            "precipitation": 0
        },
        {
            "dt": 1600275420,
            "precipitation": 0
        },
        {
            "dt": 1600275480,
            "precipitation": 0
        },
        {
            "dt": 1600275540,
            "precipitation": 0
        },
        {
            "dt": 1600275600,
            "precipitation": 0
        },
        {
            "dt": 1600275660,
            "precipitation": 0
        },
        {
            "dt": 1600275720,
            "precipitation": 0
        },
        {
            "dt": 1600275780,
            "precipitation": 0
        },
        {
            "dt": 1600275840,
            "precipitation": 0
        },
        {
            "dt": 1600275900,
            "precipitation": 0
        },
        {
            "dt": 1600275960,
            "precipitation": 0
        },
        {
            "dt": 1600276020,
            "precipitation": 0
        },
        {
            "dt": 1600276080,
            "precipitation": 0
        },
        {
            "dt": 1600276140,
            "precipitation": 0
        },
        {
            "dt": 1600276200,
            "precipitation": 0
        },
        {
            "dt": 1600276260,
            "precipitation": 0
        },
        {
            "dt": 1600276320,
            "precipitation": 0
        }
    ],
    "hourly": [
        {
            "dt": 1600272000,
            "temp": 32,
            "feels_like": 35.66,
            "pressure": 1011,
            "humidity": 59,
            "dew_point": 22.98,
            "clouds": 40,
            "visibility": 10000,
            "wind_speed": 2.23,
            "wind_deg": 43,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.09
        },
        {
            "dt": 1600275600,
            "temp": 32.69,
            "feels_like": 35.22,
            "pressure": 1010,
            "humidity": 54,
            "dew_point": 22.17,
            "clouds": 33,
            "visibility": 10000,
            "wind_speed": 3.21,
            "wind_deg": 8,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.09
        },
        {
            "dt": 1600279200,
            "temp": 32.38,
            "feels_like": 34.02,
            "pressure": 1009,
            "humidity": 54,
            "dew_point": 21.88,
            "clouds": 32,
            "visibility": 10000,
            "wind_speed": 4.27,
            "wind_deg": 6,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.05
        },
        {
            "dt": 1600282800,
            "temp": 30.79,
            "feels_like": 32.57,
            "pressure": 1007,
            "humidity": 61,
            "dew_point": 22.4,
            "clouds": 27,
            "visibility": 10000,
            "wind_speed": 4.47,
            "wind_deg": 359,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600286400,
            "temp": 29.32,
            "feels_like": 31.81,
            "pressure": 1007,
            "humidity": 69,
            "dew_point": 23.04,
            "clouds": 17,
            "visibility": 10000,
            "wind_speed": 3.96,
            "wind_deg": 346,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600290000,
            "temp": 27.99,
            "feels_like": 31.01,
            "pressure": 1007,
            "humidity": 76,
            "dew_point": 23.53,
            "clouds": 12,
            "visibility": 10000,
            "wind_speed": 3.46,
            "wind_deg": 342,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600293600,
            "temp": 27.65,
            "feels_like": 31.46,
            "pressure": 1008,
            "humidity": 79,
            "dew_point": 23.8,
            "clouds": 10,
            "visibility": 10000,
            "wind_speed": 2.59,
            "wind_deg": 344,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600297200,
            "temp": 27.54,
            "feels_like": 31.72,
            "pressure": 1009,
            "humidity": 79,
            "dew_point": 23.62,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 1.98,
            "wind_deg": 14,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600300800,
            "temp": 26.94,
            "feels_like": 29.69,
            "pressure": 1010,
            "humidity": 76,
            "dew_point": 22.56,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 3.05,
            "wind_deg": 70,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600304400,
            "temp": 26.13,
            "feels_like": 28.79,
            "pressure": 1011,
            "humidity": 79,
            "dew_point": 22.29,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.06,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600308000,
            "temp": 25.7,
            "feels_like": 28.32,
            "pressure": 1011,
            "humidity": 81,
            "dew_point": 22.3,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.12,
            "wind_deg": 63,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600311600,
            "temp": 25.36,
            "feels_like": 27.99,
            "pressure": 1011,
            "humidity": 83,
            "dew_point": 22.3,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.16,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600315200,
            "temp": 24.95,
            "feels_like": 27.58,
            "pressure": 1010,
            "humidity": 84,
            "dew_point": 22.25,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600318800,
            "temp": 24.69,
            "feels_like": 27.42,
            "pressure": 1009,
            "humidity": 86,
            "dew_point": 22.37,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.96,
            "wind_deg": 57,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600322400,
            "temp": 24.47,
            "feels_like": 27.21,
            "pressure": 1009,
            "humidity": 88,
            "dew_point": 22.37,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.06,
            "wind_deg": 58,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600326000,
            "temp": 24.44,
            "feels_like": 27.06,
            "pressure": 1009,
            "humidity": 88,
            "dew_point": 22.35,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.21,
            "wind_deg": 56,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600329600,
            "temp": 24.44,
            "feels_like": 27.08,
            "pressure": 1009,
            "humidity": 88,
            "dew_point": 22.44,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.18,
            "wind_deg": 56,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600333200,
            "temp": 24.27,
            "feels_like": 26.85,
            "pressure": 1010,
            "humidity": 89,
            "dew_point": 22.44,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.29,
            "wind_deg": 61,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600336800,
            "temp": 25.01,
            "feels_like": 27.4,
            "pressure": 1011,
            "humidity": 86,
            "dew_point": 22.52,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.69,
            "wind_deg": 62,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600340400,
            "temp": 26.77,
            "feels_like": 29.18,
            "pressure": 1011,
            "humidity": 78,
            "dew_point": 22.62,
            "clouds": 3,
            "visibility": 10000,
            "wind_speed": 3.73,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600344000,
            "temp": 28.83,
            "feels_like": 31.48,
            "pressure": 1012,
            "humidity": 68,
            "dew_point": 22.5,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 3.18,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600347600,
            "temp": 30.94,
            "feels_like": 33.69,
            "pressure": 1012,
            "humidity": 59,
            "dew_point": 22.18,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.77,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600351200,
            "temp": 32.76,
            "feels_like": 35.72,
            "pressure": 1012,
            "humidity": 53,
            "dew_point": 21.98,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.41,
            "wind_deg": 51,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600354800,
            "temp": 33.82,
            "feels_like": 36.52,
            "pressure": 1011,
            "humidity": 49,
            "dew_point": 21.66,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.55,
            "wind_deg": 27,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600358400,
            "temp": 33.08,
            "feels_like": 35.43,
            "pressure": 1010,
            "humidity": 54,
            "dew_point": 22.58,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.74,
            "wind_deg": 3,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.2,
            "rain": {
                "1h": 0.11
            }
        },
        {
            "dt": 1600362000,
            "temp": 29.94,
            "feels_like": 32.52,
            "pressure": 1008,
            "humidity": 70,
            "dew_point": 24.02,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 4.51,
            "wind_deg": 344,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.28,
            "rain": {
                "1h": 0.97
            }
        },
        {
            "dt": 1600365600,
            "temp": 28.97,
            "feels_like": 31.82,
            "pressure": 1008,
            "humidity": 76,
            "dew_point": 24.47,
            "clouds": 8,
            "visibility": 10000,
            "wind_speed": 4.49,
            "wind_deg": 344,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.28,
            "rain": {
                "1h": 0.63
            }
        },
        {
            "dt": 1600369200,
            "temp": 28.35,
            "feels_like": 30.87,
            "pressure": 1007,
            "humidity": 77,
            "dew_point": 23.96,
            "clouds": 72,
            "visibility": 10000,
            "wind_speed": 4.64,
            "wind_deg": 352,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.4,
            "rain": {
                "1h": 0.74
            }
        },
        {
            "dt": 1600372800,
            "temp": 28.33,
            "feels_like": 30.93,
            "pressure": 1008,
            "humidity": 77,
            "dew_point": 23.97,
            "clouds": 46,
            "visibility": 10000,
            "wind_speed": 4.51,
            "wind_deg": 354,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.4,
            "rain": {
                "1h": 0.51
            }
        },
        {
            "dt": 1600376400,
            "temp": 27.94,
            "feels_like": 31.28,
            "pressure": 1009,
            "humidity": 78,
            "dew_point": 23.84,
            "clouds": 37,
            "visibility": 10000,
            "wind_speed": 3.32,
            "wind_deg": 11,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.36,
            "rain": {
                "1h": 0.41
            }
        },
        {
            "dt": 1600380000,
            "temp": 27.21,
            "feels_like": 30.41,
            "pressure": 1009,
            "humidity": 78,
            "dew_point": 23.2,
            "clouds": 39,
            "visibility": 10000,
            "wind_speed": 2.95,
            "wind_deg": 68,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.32,
            "rain": {
                "1h": 0.12
            }
        },
        {
            "dt": 1600383600,
            "temp": 26.42,
            "feels_like": 29.22,
            "pressure": 1010,
            "humidity": 79,
            "dew_point": 22.64,
            "clouds": 31,
            "visibility": 10000,
            "wind_speed": 3.08,
            "wind_deg": 60,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0.12
        },
        {
            "dt": 1600387200,
            "temp": 26.2,
            "feels_like": 28.8,
            "pressure": 1011,
            "humidity": 80,
            "dew_point": 22.62,
            "clouds": 26,
            "visibility": 10000,
            "wind_speed": 3.36,
            "wind_deg": 58,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0.12
        },
        {
            "dt": 1600390800,
            "temp": 25.7,
            "feels_like": 28.12,
            "pressure": 1012,
            "humidity": 82,
            "dew_point": 22.41,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.56,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600394400,
            "temp": 25.24,
            "feels_like": 27.65,
            "pressure": 1012,
            "humidity": 84,
            "dew_point": 22.37,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.53,
            "wind_deg": 68,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600398000,
            "temp": 25.02,
            "feels_like": 27.64,
            "pressure": 1011,
            "humidity": 86,
            "dew_point": 22.52,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.36,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600401600,
            "temp": 24.94,
            "feels_like": 27.68,
            "pressure": 1011,
            "humidity": 87,
            "dew_point": 22.65,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.28,
            "wind_deg": 56,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600405200,
            "temp": 24.92,
            "feels_like": 27.71,
            "pressure": 1010,
            "humidity": 87,
            "dew_point": 22.64,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.19,
            "wind_deg": 48,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600408800,
            "temp": 24.75,
            "feels_like": 27.54,
            "pressure": 1009,
            "humidity": 87,
            "dew_point": 22.52,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.06,
            "wind_deg": 46,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600412400,
            "temp": 24.48,
            "feels_like": 27.34,
            "pressure": 1009,
            "humidity": 88,
            "dew_point": 22.44,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.9,
            "wind_deg": 55,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600416000,
            "temp": 24.22,
            "feels_like": 27.18,
            "pressure": 1009,
            "humidity": 90,
            "dew_point": 22.55,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.85,
            "wind_deg": 60,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600419600,
            "temp": 23.86,
            "feels_like": 26.92,
            "pressure": 1010,
            "humidity": 91,
            "dew_point": 22.45,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 2.57,
            "wind_deg": 64,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600423200,
            "temp": 24.79,
            "feels_like": 27.58,
            "pressure": 1011,
            "humidity": 87,
            "dew_point": 22.64,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 3.09,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600426800,
            "temp": 26.7,
            "feels_like": 29.35,
            "pressure": 1012,
            "humidity": 79,
            "dew_point": 22.9,
            "clouds": 15,
            "visibility": 10000,
            "wind_speed": 3.51,
            "wind_deg": 60,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600430400,
            "temp": 28.32,
            "feels_like": 31.28,
            "pressure": 1012,
            "humidity": 72,
            "dew_point": 22.8,
            "clouds": 22,
            "visibility": 10000,
            "wind_speed": 3.08,
            "wind_deg": 57,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600434000,
            "temp": 30.48,
            "feels_like": 33.55,
            "pressure": 1013,
            "humidity": 62,
            "dew_point": 22.52,
            "clouds": 22,
            "visibility": 10000,
            "wind_speed": 2.61,
            "wind_deg": 55,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600437600,
            "temp": 32.33,
            "feels_like": 35.4,
            "pressure": 1012,
            "humidity": 55,
            "dew_point": 22.22,
            "clouds": 11,
            "visibility": 10000,
            "wind_speed": 2.41,
            "wind_deg": 33,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600441200,
            "temp": 32.98,
            "feels_like": 35.45,
            "pressure": 1012,
            "humidity": 53,
            "dew_point": 22.34,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 3.27,
            "wind_deg": 2,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        }
    ],
    "daily": [
        {
            "dt": 1600268400,
            "sunrise": 1600247143,
            "sunset": 1600290712,
            "temp": {
                "day": 32,
                "min": 23.72,
                "max": 32.23,
                "night": 27.26,
                "eve": 28.86,
                "morn": 23.72
            },
            "feels_like": {
                "day": 35.77,
                "night": 30.06,
                "eve": 31.98,
                "morn": 26.53
            },
            "pressure": 1011,
            "humidity": 59,
            "dew_point": 22.98,
            "wind_speed": 2.08,
            "wind_deg": 80,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "clouds": 40,
            "pop": 0.05,
            "uvi": 13.22
        },
        {
            "dt": 1600354800,
            "sunrise": 1600333519,
            "sunset": 1600377093,
            "temp": {
                "day": 33.82,
                "min": 24.27,
                "max": 33.82,
                "night": 26.2,
                "eve": 27.94,
                "morn": 24.27
            },
            "feels_like": {
                "day": 36.52,
                "night": 28.8,
                "eve": 31.28,
                "morn": 26.85
            },
            "pressure": 1011,
            "humidity": 49,
            "dew_point": 21.66,
            "wind_speed": 2.55,
            "wind_deg": 27,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 0,
            "pop": 0.36,
            "rain": 3.49,
            "uvi": 12.72
        },
        {
            "dt": 1600441200,
            "sunrise": 1600419894,
            "sunset": 1600463473,
            "temp": {
                "day": 32.98,
                "min": 23.86,
                "max": 32.98,
                "night": 24.63,
                "eve": 26.1,
                "morn": 23.86
            },
            "feels_like": {
                "day": 35.45,
                "night": 27.4,
                "eve": 30.34,
                "morn": 26.92
            },
            "pressure": 1012,
            "humidity": 53,
            "dew_point": 22.34,
            "wind_speed": 3.27,
            "wind_deg": 2,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 7,
            "pop": 0.61,
            "rain": 6.7,
            "uvi": 12.35
        },
        {
            "dt": 1600527600,
            "sunrise": 1600506269,
            "sunset": 1600549853,
            "temp": {
                "day": 32.3,
                "min": 23.9,
                "max": 32.3,
                "night": 26.59,
                "eve": 27.75,
                "morn": 23.9
            },
            "feels_like": {
                "day": 34.38,
                "night": 28.75,
                "eve": 30.52,
                "morn": 27.02
            },
            "pressure": 1011,
            "humidity": 57,
            "dew_point": 22.78,
            "wind_speed": 4.26,
            "wind_deg": 353,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 2,
            "pop": 0.31,
            "rain": 1.73,
            "uvi": 13.24
        },
        {
            "dt": 1600614000,
            "sunrise": 1600592645,
            "sunset": 1600636233,
            "temp": {
                "day": 32.55,
                "min": 23.51,
                "max": 32.55,
                "night": 26.43,
                "eve": 27.66,
                "morn": 23.51
            },
            "feels_like": {
                "day": 34.59,
                "night": 28.67,
                "eve": 29.09,
                "morn": 26.5
            },
            "pressure": 1010,
            "humidity": 53,
            "dew_point": 21.76,
            "wind_speed": 3.58,
            "wind_deg": 357,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 0,
            "pop": 0.41,
            "rain": 0.61,
            "uvi": 13.85
        },
        {
            "dt": 1600700400,
            "sunrise": 1600679021,
            "sunset": 1600722613,
            "temp": {
                "day": 30.32,
                "min": 23.98,
                "max": 31.87,
                "night": 26.72,
                "eve": 28.24,
                "morn": 24.15
            },
            "feels_like": {
                "day": 33.15,
                "night": 28.23,
                "eve": 31.54,
                "morn": 26.72
            },
            "pressure": 1012,
            "humidity": 60,
            "dew_point": 21.82,
            "wind_speed": 2.43,
            "wind_deg": 79,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 90,
            "pop": 0.28,
            "rain": 0.35,
            "uvi": 13.42
        },
        {
            "dt": 1600786800,
            "sunrise": 1600765396,
            "sunset": 1600808994,
            "temp": {
                "day": 33.52,
                "min": 22.82,
                "max": 33.52,
                "night": 26.79,
                "eve": 28.9,
                "morn": 22.82
            },
            "feels_like": {
                "day": 35.39,
                "night": 28.01,
                "eve": 32.35,
                "morn": 25.11
            },
            "pressure": 1012,
            "humidity": 45,
            "dew_point": 20.27,
            "wind_speed": 2.56,
            "wind_deg": 30,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 1,
            "pop": 0.2,
            "rain": 0.17,
            "uvi": 13.85
        },
        {
            "dt": 1600873200,
            "sunrise": 1600851772,
            "sunset": 1600895374,
            "temp": {
                "day": 33.69,
                "min": 23,
                "max": 33.69,
                "night": 26.25,
                "eve": 27.62,
                "morn": 23
            },
            "feels_like": {
                "day": 35.9,
                "night": 28.94,
                "eve": 29.91,
                "morn": 25.77
            },
            "pressure": 1012,
            "humidity": 44,
            "dew_point": 19.81,
            "wind_speed": 1.93,
            "wind_deg": 25,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 70,
            "pop": 0.39,
            "rain": 0.48,
            "uvi": 13.45
        }
    ]
},
{
    "lat": -14.87,
    "lon": -40.84,
    "timezone": "America/Bahia",
    "timezone_offset": -10800,
    "current": {
        "dt": 1600272709,
        "sunrise": 1600245438,
        "sunset": 1600288742,
        "temp": 26.98,
        "feels_like": 24.53,
        "pressure": 1016,
        "humidity": 37,
        "dew_point": 11.06,
        "uvi": 12.58,
        "clouds": 18,
        "visibility": 10000,
        "wind_speed": 3.98,
        "wind_deg": 100,
        "weather": [
            {
                "id": 801,
                "main": "Clouds",
                "description": "few clouds",
                "icon": "02d"
            }
        ]
    },
    "minutely": [
        {
            "dt": 1600272720,
            "precipitation": 0
        },
        {
            "dt": 1600272780,
            "precipitation": 0
        },
        {
            "dt": 1600272840,
            "precipitation": 0
        },
        {
            "dt": 1600272900,
            "precipitation": 0
        },
        {
            "dt": 1600272960,
            "precipitation": 0
        },
        {
            "dt": 1600273020,
            "precipitation": 0
        },
        {
            "dt": 1600273080,
            "precipitation": 0
        },
        {
            "dt": 1600273140,
            "precipitation": 0
        },
        {
            "dt": 1600273200,
            "precipitation": 0
        },
        {
            "dt": 1600273260,
            "precipitation": 0
        },
        {
            "dt": 1600273320,
            "precipitation": 0
        },
        {
            "dt": 1600273380,
            "precipitation": 0
        },
        {
            "dt": 1600273440,
            "precipitation": 0
        },
        {
            "dt": 1600273500,
            "precipitation": 0
        },
        {
            "dt": 1600273560,
            "precipitation": 0
        },
        {
            "dt": 1600273620,
            "precipitation": 0
        },
        {
            "dt": 1600273680,
            "precipitation": 0
        },
        {
            "dt": 1600273740,
            "precipitation": 0
        },
        {
            "dt": 1600273800,
            "precipitation": 0
        },
        {
            "dt": 1600273860,
            "precipitation": 0
        },
        {
            "dt": 1600273920,
            "precipitation": 0
        },
        {
            "dt": 1600273980,
            "precipitation": 0
        },
        {
            "dt": 1600274040,
            "precipitation": 0
        },
        {
            "dt": 1600274100,
            "precipitation": 0
        },
        {
            "dt": 1600274160,
            "precipitation": 0
        },
        {
            "dt": 1600274220,
            "precipitation": 0
        },
        {
            "dt": 1600274280,
            "precipitation": 0
        },
        {
            "dt": 1600274340,
            "precipitation": 0
        },
        {
            "dt": 1600274400,
            "precipitation": 0
        },
        {
            "dt": 1600274460,
            "precipitation": 0
        },
        {
            "dt": 1600274520,
            "precipitation": 0
        },
        {
            "dt": 1600274580,
            "precipitation": 0
        },
        {
            "dt": 1600274640,
            "precipitation": 0
        },
        {
            "dt": 1600274700,
            "precipitation": 0
        },
        {
            "dt": 1600274760,
            "precipitation": 0
        },
        {
            "dt": 1600274820,
            "precipitation": 0
        },
        {
            "dt": 1600274880,
            "precipitation": 0
        },
        {
            "dt": 1600274940,
            "precipitation": 0
        },
        {
            "dt": 1600275000,
            "precipitation": 0
        },
        {
            "dt": 1600275060,
            "precipitation": 0
        },
        {
            "dt": 1600275120,
            "precipitation": 0
        },
        {
            "dt": 1600275180,
            "precipitation": 0
        },
        {
            "dt": 1600275240,
            "precipitation": 0
        },
        {
            "dt": 1600275300,
            "precipitation": 0
        },
        {
            "dt": 1600275360,
            "precipitation": 0
        },
        {
            "dt": 1600275420,
            "precipitation": 0
        },
        {
            "dt": 1600275480,
            "precipitation": 0
        },
        {
            "dt": 1600275540,
            "precipitation": 0
        },
        {
            "dt": 1600275600,
            "precipitation": 0
        },
        {
            "dt": 1600275660,
            "precipitation": 0
        },
        {
            "dt": 1600275720,
            "precipitation": 0
        },
        {
            "dt": 1600275780,
            "precipitation": 0
        },
        {
            "dt": 1600275840,
            "precipitation": 0
        },
        {
            "dt": 1600275900,
            "precipitation": 0
        },
        {
            "dt": 1600275960,
            "precipitation": 0
        },
        {
            "dt": 1600276020,
            "precipitation": 0
        },
        {
            "dt": 1600276080,
            "precipitation": 0
        },
        {
            "dt": 1600276140,
            "precipitation": 0
        },
        {
            "dt": 1600276200,
            "precipitation": 0
        },
        {
            "dt": 1600276260,
            "precipitation": 0
        },
        {
            "dt": 1600276320,
            "precipitation": 0
        }
    ],
    "hourly": [
        {
            "dt": 1600272000,
            "temp": 26.98,
            "feels_like": 24.53,
            "pressure": 1016,
            "humidity": 37,
            "dew_point": 11.06,
            "clouds": 18,
            "visibility": 10000,
            "wind_speed": 3.98,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600275600,
            "temp": 27.08,
            "feels_like": 24.52,
            "pressure": 1016,
            "humidity": 36,
            "dew_point": 10.74,
            "clouds": 16,
            "visibility": 10000,
            "wind_speed": 4,
            "wind_deg": 104,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600279200,
            "temp": 26.93,
            "feels_like": 24.05,
            "pressure": 1015,
            "humidity": 34,
            "dew_point": 9.75,
            "clouds": 13,
            "visibility": 10000,
            "wind_speed": 4.08,
            "wind_deg": 104,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600282800,
            "temp": 25.94,
            "feels_like": 23.01,
            "pressure": 1015,
            "humidity": 37,
            "dew_point": 10.15,
            "clouds": 1,
            "visibility": 10000,
            "wind_speed": 4.29,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600286400,
            "temp": 23.88,
            "feels_like": 21.5,
            "pressure": 1015,
            "humidity": 46,
            "dew_point": 11.57,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 4.09,
            "wind_deg": 96,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600290000,
            "temp": 20.7,
            "feels_like": 19.05,
            "pressure": 1016,
            "humidity": 59,
            "dew_point": 12.54,
            "clouds": 6,
            "visibility": 10000,
            "wind_speed": 3.42,
            "wind_deg": 92,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600293600,
            "temp": 19.07,
            "feels_like": 17.55,
            "pressure": 1016,
            "humidity": 69,
            "dew_point": 13.36,
            "clouds": 6,
            "visibility": 10000,
            "wind_speed": 3.62,
            "wind_deg": 94,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600297200,
            "temp": 17.86,
            "feels_like": 16.27,
            "pressure": 1017,
            "humidity": 80,
            "dew_point": 14.54,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 4.25,
            "wind_deg": 93,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600300800,
            "temp": 16.62,
            "feels_like": 15.32,
            "pressure": 1018,
            "humidity": 90,
            "dew_point": 15,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 4.15,
            "wind_deg": 92,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600304400,
            "temp": 15.86,
            "feels_like": 14.6,
            "pressure": 1019,
            "humidity": 93,
            "dew_point": 14.85,
            "clouds": 13,
            "visibility": 10000,
            "wind_speed": 3.97,
            "wind_deg": 95,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600308000,
            "temp": 15.34,
            "feels_like": 14.3,
            "pressure": 1019,
            "humidity": 95,
            "dew_point": 14.57,
            "clouds": 11,
            "visibility": 10000,
            "wind_speed": 3.56,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600311600,
            "temp": 14.93,
            "feels_like": 13.9,
            "pressure": 1019,
            "humidity": 96,
            "dew_point": 14.4,
            "clouds": 13,
            "visibility": 10000,
            "wind_speed": 3.42,
            "wind_deg": 104,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600315200,
            "temp": 14.81,
            "feels_like": 13.82,
            "pressure": 1018,
            "humidity": 97,
            "dew_point": 14.39,
            "clouds": 20,
            "visibility": 10000,
            "wind_speed": 3.39,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600318800,
            "temp": 14.75,
            "feels_like": 13.82,
            "pressure": 1018,
            "humidity": 96,
            "dew_point": 14.12,
            "clouds": 27,
            "visibility": 10000,
            "wind_speed": 3.2,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600322400,
            "temp": 14.59,
            "feels_like": 13.75,
            "pressure": 1018,
            "humidity": 95,
            "dew_point": 13.82,
            "clouds": 33,
            "visibility": 10000,
            "wind_speed": 2.91,
            "wind_deg": 98,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600326000,
            "temp": 14.14,
            "feels_like": 13.65,
            "pressure": 1018,
            "humidity": 95,
            "dew_point": 13.48,
            "clouds": 51,
            "visibility": 10000,
            "wind_speed": 2.2,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600329600,
            "temp": 13.2,
            "feels_like": 13,
            "pressure": 1018,
            "humidity": 99,
            "dew_point": 13.07,
            "clouds": 46,
            "visibility": 10000,
            "wind_speed": 1.64,
            "wind_deg": 106,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600333200,
            "temp": 13.28,
            "feels_like": 13.1,
            "pressure": 1019,
            "humidity": 98,
            "dew_point": 13.09,
            "clouds": 45,
            "visibility": 8919,
            "wind_speed": 1.58,
            "wind_deg": 104,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600336800,
            "temp": 15.17,
            "feels_like": 14.89,
            "pressure": 1020,
            "humidity": 87,
            "dew_point": 13.14,
            "clouds": 50,
            "visibility": 10000,
            "wind_speed": 1.74,
            "wind_deg": 106,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600340400,
            "temp": 17.73,
            "feels_like": 16.73,
            "pressure": 1020,
            "humidity": 68,
            "dew_point": 11.83,
            "clouds": 52,
            "visibility": 10000,
            "wind_speed": 2.21,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600344000,
            "temp": 21.02,
            "feels_like": 18.93,
            "pressure": 1020,
            "humidity": 47,
            "dew_point": 9.57,
            "clouds": 46,
            "visibility": 10000,
            "wind_speed": 2.78,
            "wind_deg": 117,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600347600,
            "temp": 23.73,
            "feels_like": 20.73,
            "pressure": 1019,
            "humidity": 34,
            "dew_point": 7.01,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.27,
            "wind_deg": 117,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600351200,
            "temp": 25.5,
            "feels_like": 21.89,
            "pressure": 1018,
            "humidity": 27,
            "dew_point": 5.24,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.58,
            "wind_deg": 116,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600354800,
            "temp": 26.75,
            "feels_like": 22.92,
            "pressure": 1016,
            "humidity": 23,
            "dew_point": 4.31,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.56,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600358400,
            "temp": 27.5,
            "feels_like": 23.66,
            "pressure": 1014,
            "humidity": 21,
            "dew_point": 3.6,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.39,
            "wind_deg": 111,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600362000,
            "temp": 27.8,
            "feels_like": 23.91,
            "pressure": 1013,
            "humidity": 20,
            "dew_point": 3.32,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.36,
            "wind_deg": 112,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600365600,
            "temp": 27.32,
            "feels_like": 23.46,
            "pressure": 1012,
            "humidity": 22,
            "dew_point": 3.93,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.55,
            "wind_deg": 114,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600369200,
            "temp": 26.23,
            "feels_like": 22.41,
            "pressure": 1012,
            "humidity": 25,
            "dew_point": 4.92,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.75,
            "wind_deg": 112,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600372800,
            "temp": 24.05,
            "feels_like": 20.7,
            "pressure": 1013,
            "humidity": 32,
            "dew_point": 6.44,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.58,
            "wind_deg": 105,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600376400,
            "temp": 20.23,
            "feels_like": 17.24,
            "pressure": 1014,
            "humidity": 43,
            "dew_point": 7.36,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.35,
            "wind_deg": 94,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600380000,
            "temp": 18.74,
            "feels_like": 15.9,
            "pressure": 1015,
            "humidity": 48,
            "dew_point": 7.6,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.22,
            "wind_deg": 90,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600383600,
            "temp": 17.87,
            "feels_like": 15.15,
            "pressure": 1016,
            "humidity": 53,
            "dew_point": 8.35,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.27,
            "wind_deg": 86,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600387200,
            "temp": 16.4,
            "feels_like": 14.36,
            "pressure": 1017,
            "humidity": 69,
            "dew_point": 10.84,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.26,
            "wind_deg": 80,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600390800,
            "temp": 15.22,
            "feels_like": 14.06,
            "pressure": 1017,
            "humidity": 81,
            "dew_point": 11.99,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.53,
            "wind_deg": 78,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600394400,
            "temp": 14.36,
            "feels_like": 13.69,
            "pressure": 1017,
            "humidity": 87,
            "dew_point": 12.27,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 1.94,
            "wind_deg": 79,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600398000,
            "temp": 13.62,
            "feels_like": 13.25,
            "pressure": 1017,
            "humidity": 91,
            "dew_point": 12.34,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 1.49,
            "wind_deg": 85,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600401600,
            "temp": 13.14,
            "feels_like": 12.95,
            "pressure": 1017,
            "humidity": 94,
            "dew_point": 12.29,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 1.24,
            "wind_deg": 84,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600405200,
            "temp": 12.73,
            "feels_like": 12.47,
            "pressure": 1016,
            "humidity": 96,
            "dew_point": 12.26,
            "clouds": 2,
            "visibility": 10000,
            "wind_speed": 1.3,
            "wind_deg": 67,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600408800,
            "temp": 12.56,
            "feels_like": 12.34,
            "pressure": 1016,
            "humidity": 98,
            "dew_point": 12.31,
            "clouds": 4,
            "visibility": 10000,
            "wind_speed": 1.31,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600412400,
            "temp": 12.57,
            "feels_like": 12.35,
            "pressure": 1016,
            "humidity": 98,
            "dew_point": 12.36,
            "clouds": 29,
            "visibility": 10000,
            "wind_speed": 1.32,
            "wind_deg": 62,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600416000,
            "temp": 12.52,
            "feels_like": 12.27,
            "pressure": 1016,
            "humidity": 98,
            "dew_point": 12.28,
            "clouds": 34,
            "visibility": 10000,
            "wind_speed": 1.34,
            "wind_deg": 62,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600419600,
            "temp": 12.71,
            "feels_like": 12.5,
            "pressure": 1017,
            "humidity": 97,
            "dew_point": 12.39,
            "clouds": 36,
            "visibility": 10000,
            "wind_speed": 1.3,
            "wind_deg": 56,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600423200,
            "temp": 15.4,
            "feels_like": 15.04,
            "pressure": 1017,
            "humidity": 84,
            "dew_point": 12.76,
            "clouds": 37,
            "visibility": 10000,
            "wind_speed": 1.72,
            "wind_deg": 39,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600426800,
            "temp": 18.87,
            "feels_like": 18.06,
            "pressure": 1018,
            "humidity": 65,
            "dew_point": 12.24,
            "clouds": 33,
            "visibility": 10000,
            "wind_speed": 2.11,
            "wind_deg": 41,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600430400,
            "temp": 22.29,
            "feels_like": 20.94,
            "pressure": 1017,
            "humidity": 49,
            "dew_point": 11.1,
            "clouds": 27,
            "visibility": 10000,
            "wind_speed": 2.41,
            "wind_deg": 50,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600434000,
            "temp": 25.24,
            "feels_like": 23.58,
            "pressure": 1017,
            "humidity": 38,
            "dew_point": 9.98,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.4,
            "wind_deg": 63,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600437600,
            "temp": 27.77,
            "feels_like": 25.98,
            "pressure": 1016,
            "humidity": 31,
            "dew_point": 9.27,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.28,
            "wind_deg": 86,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1600441200,
            "temp": 29.49,
            "feels_like": 27.34,
            "pressure": 1014,
            "humidity": 27,
            "dew_point": 8.69,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.59,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0
        }
    ],
    "daily": [
        {
            "dt": 1600264800,
            "sunrise": 1600245438,
            "sunset": 1600288742,
            "temp": {
                "day": 26.98,
                "min": 15.53,
                "max": 26.98,
                "night": 17.28,
                "eve": 22.06,
                "morn": 15.53
            },
            "feels_like": {
                "day": 24.58,
                "night": 16.02,
                "eve": 20.47,
                "morn": 14.99
            },
            "pressure": 1016,
            "humidity": 37,
            "dew_point": 11.06,
            "wind_speed": 3.91,
            "wind_deg": 100,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "clouds": 18,
            "pop": 0,
            "uvi": 12.58
        },
        {
            "dt": 1600351200,
            "sunrise": 1600331791,
            "sunset": 1600375144,
            "temp": {
                "day": 26.75,
                "min": 13.28,
                "max": 27.32,
                "night": 16.4,
                "eve": 20.23,
                "morn": 13.28
            },
            "feels_like": {
                "day": 22.92,
                "night": 14.36,
                "eve": 17.24,
                "morn": 13.1
            },
            "pressure": 1016,
            "humidity": 23,
            "dew_point": 4.31,
            "wind_speed": 3.56,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 0,
            "pop": 0,
            "uvi": 12.39
        },
        {
            "dt": 1600437600,
            "sunrise": 1600418144,
            "sunset": 1600461547,
            "temp": {
                "day": 29.49,
                "min": 12.56,
                "max": 29.49,
                "night": 17.52,
                "eve": 21.36,
                "morn": 12.71
            },
            "feels_like": {
                "day": 27.34,
                "night": 15.92,
                "eve": 18.84,
                "morn": 12.5
            },
            "pressure": 1014,
            "humidity": 27,
            "dew_point": 8.69,
            "wind_speed": 2.59,
            "wind_deg": 113,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 0,
            "pop": 0,
            "uvi": 12.01
        },
        {
            "dt": 1600524000,
            "sunrise": 1600504497,
            "sunset": 1600547949,
            "temp": {
                "day": 29.7,
                "min": 12.77,
                "max": 29.7,
                "night": 16.76,
                "eve": 20.97,
                "morn": 14.11
            },
            "feels_like": {
                "day": 26.65,
                "night": 15.4,
                "eve": 18.68,
                "morn": 13.35
            },
            "pressure": 1014,
            "humidity": 22,
            "dew_point": 6.2,
            "wind_speed": 2.95,
            "wind_deg": 94,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 0,
            "pop": 0,
            "uvi": 11.74
        },
        {
            "dt": 1600610400,
            "sunrise": 1600590851,
            "sunset": 1600634352,
            "temp": {
                "day": 30.57,
                "min": 13.25,
                "max": 30.91,
                "night": 17.54,
                "eve": 22.27,
                "morn": 13.55
            },
            "feels_like": {
                "day": 27.59,
                "night": 16.9,
                "eve": 19.73,
                "morn": 13.16
            },
            "pressure": 1013,
            "humidity": 22,
            "dew_point": 6.47,
            "wind_speed": 3.07,
            "wind_deg": 67,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "clouds": 0,
            "pop": 0,
            "uvi": 11.97
        },
        {
            "dt": 1600696800,
            "sunrise": 1600677204,
            "sunset": 1600720754,
            "temp": {
                "day": 28.17,
                "min": 15.82,
                "max": 28.4,
                "night": 18.08,
                "eve": 21.64,
                "morn": 16.85
            },
            "feels_like": {
                "day": 26.28,
                "night": 17.26,
                "eve": 20.65,
                "morn": 16.93
            },
            "pressure": 1015,
            "humidity": 43,
            "dew_point": 14.78,
            "wind_speed": 4.7,
            "wind_deg": 79,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 67,
            "pop": 0.02,
            "uvi": 11.99
        },
        {
            "dt": 1600783200,
            "sunrise": 1600763557,
            "sunset": 1600807157,
            "temp": {
                "day": 27.48,
                "min": 16.91,
                "max": 27.48,
                "night": 17.96,
                "eve": 21.16,
                "morn": 18.04
            },
            "feels_like": {
                "day": 25.53,
                "night": 17.07,
                "eve": 20.48,
                "morn": 17.48
            },
            "pressure": 1017,
            "humidity": 45,
            "dew_point": 14.5,
            "wind_speed": 4.83,
            "wind_deg": 92,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "clouds": 30,
            "pop": 0.12,
            "uvi": 11.99
        },
        {
            "dt": 1600869600,
            "sunrise": 1600849910,
            "sunset": 1600893560,
            "temp": {
                "day": 24.75,
                "min": 15.24,
                "max": 26.36,
                "night": 17.33,
                "eve": 20.63,
                "morn": 15.24
            },
            "feels_like": {
                "day": 23.35,
                "night": 15.89,
                "eve": 19.4,
                "morn": 15.12
            },
            "pressure": 1019,
            "humidity": 50,
            "dew_point": 13.65,
            "wind_speed": 3.62,
            "wind_deg": 103,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 79,
            "pop": 0,
            "uvi": 12.82
        }
    ]
}
]
