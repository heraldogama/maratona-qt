import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Controls.Material 2.15
import QtCharts 2.15

import "FontAwesome"
import "restful-dashboard.js" as JS
import "offline-data.js" as OD

ApplicationWindow {
    visible: true
    width: 1024; height: 768
    title: qsTr("QML Weather Dashboard")

    Material.theme: Material.Dark
    Material.accent: "#03aced"

    property var currentWeather
    property var detailedWeather

    Material.background: "#141414"

    ColumnLayout {
        id: mainLayout
        anchors { fill: parent; margins: 10 }
        ComboBox {
            id: cityComboBox
            Layout.fillWidth: true
            model: [ "Salvador", "São Paulo", "Belém", "Vitória da Conquista" ]
            onCurrentValueChanged: JS.updateWeather(currentValue)
        }
        Flickable {
            Layout.fillWidth: true; Layout.fillHeight: true
            contentWidth: parent.width; contentHeight: gridLayout.height
            clip: true
            GridLayout {
                id: gridLayout
                width: parent.width
                columns: mainLayout.width > mainLayout.height ? 2:1
                Frame {
                    Layout.preferredWidth: parent.width > parent.height ? parent.parent.width/2:parent.width
                    Layout.alignment: Qt.AlignTop
                    Material.background: "#252525"; Material.elevation: 3
                    clip: true
                    visible: currentWeather !== undefined && detailedWeather !== undefined
                    ColumnLayout {
                        width: parent.width
                        RowLayout {
                            Layout.preferredWidth: parent.width
                            Label {
                                color: Material.accent
                                text: currentWeather ? (currentWeather.main.temp + "\u00B0"):""
                                font { pixelSize: 4.5 * refLabel.font.pixelSize; bold: true }
                            }
                            Image {
                                id: weatherImage
                                Layout.preferredHeight: parent.height; Layout.preferredWidth: parent.height
                                Layout.alignment: Qt.AlignRight
                                SequentialAnimation {
                                    running: true
                                    loops: Animation.Infinite
                                    NumberAnimation { target: weatherImage; property: "scale"; duration: 500; to: 1.2 }
                                    NumberAnimation { target: weatherImage; property: "scale"; duration: 500; to: 1 }
                                }
                            }
                        }
                        Label { id: refLabel; color: "#727271"; font { capitalization: Font.AllUppercase; bold: true } text: currentWeather ? currentWeather.weather[0].description:"" }
                        Label { text: "H:" + (currentWeather ? currentWeather.main.temp_max:"") + "\u00B0    L:" + (currentWeather ? currentWeather.main.temp_min:"") + "\u00B0"; font.bold: true; Layout.topMargin: -5 }
                        GridLayout {
                            Layout.fillWidth: true
                            Layout.topMargin: 20
                            columns: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? 6:(mainLayout.width > mainLayout.height ? 12:6)
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faTint }
                            Label { text: (currentWeather ? currentWeather.main.humidity:"") + "%"; color: "#727271"; font.bold: true }
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faWind; Layout.leftMargin: 30 }
                            Label { text: (currentWeather ? currentWeather.wind.speed:"") + "KMH"; color: "#727271"; font.bold: true }
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faCompass; Layout.leftMargin: 30 }
                            Label { text: (currentWeather ? currentWeather.wind.deg:"") + "\u00B0"; color: "#727271"; font.bold: true }
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faWeightHanging; Layout.leftMargin: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? 0:(mainLayout.width > mainLayout.height ? 30:0) }
                            Label { text: (currentWeather ? currentWeather.main.pressure:"") + "hPa"; color: "#727271"; font.bold: true }
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faSun; Layout.leftMargin: 30 }
                            Label { text: (currentWeather ? Date.fromLocaleString((new Date(currentWeather.sys.sunrise*1000)).toLocaleString()).toLocaleString(Qt.locale("en_US"), "hh:mm"):""); color: "#727271"; font.bold: true }
                            Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faMoon; Layout.leftMargin: 30 }
                            Label { text: (currentWeather ? Date.fromLocaleString((new Date(currentWeather.sys.sunset*1000)).toLocaleString()).toLocaleString(Qt.locale("en_US"), "hh:mm"):""); color: "#727271"; font.bold: true }
                        }
                        ListView {
                            Layout.fillWidth: true; Layout.topMargin: 20
                            height: children[0].childrenRect.height
                            onHeightChanged: implicitHeight = height
                            orientation: ListView.Horizontal
                            spacing: 1; clip: true
                            model: detailedWeather ? detailedWeather.daily:undefined
                            delegate: Frame {
                                Material.background: "#363636"; Material.elevation: 1
                                ColumnLayout {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    Label { Layout.alignment: Qt.AlignHCenter; font { capitalization: Font.AllUppercase; bold: true } text: Date.fromLocaleDateString((new Date(modelData.dt*1000)).toLocaleDateString()).toLocaleString(Qt.locale("en_US"), "ddd") }
                                    Label { Layout.alignment: Qt.AlignHCenter; font.bold: true; color: Material.accent; text: modelData.temp.max + "\u00B0/" + modelData.temp.min + "\u00B0" }
                                    RowLayout {
                                        Layout.alignment: Qt.AlignHCenter
                                        Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faCloudShowersHeavy }
                                        Label { text: Math.trunc(modelData.pop*100) + "%"; color: "#727271"; font.bold: true }
                                    }
                                    Image {
                                        Layout.minimumWidth: 100; Layout.minimumHeight: 100
                                        Layout.alignment: Qt.AlignHCenter
                                        source: ((JS.appid !== "") ? "http://openweathermap.org/img/wn/":"icons/") + modelData.weather[0].icon + "@2x.png"
                                        fillMode: Image.PreserveAspectCrop
                                    }
                                    Label { Layout.preferredWidth: implicitWidth*0.75; Layout.alignment: Qt.AlignHCenter; font { capitalization: Font.AllUppercase; pixelSize: 0.71 * refLabel.font.pixelSize; bold: true } text: modelData.weather[0].description; color: "#727271"; wrapMode: Text.WordWrap; horizontalAlignment: Qt.AlignHCenter }
                                }
                            }
                        }
                    }
                }
                Frame {
                    Layout.preferredWidth: parent.width > parent.height ? parent.parent.width/2:parent.width
                    Layout.alignment: Qt.AlignTop
                    Material.background: "#252525"; Material.elevation: 3
                    clip: true
                    visible: currentWeather !== undefined && detailedWeather !== undefined
                    ColumnLayout {
                        width: parent.width
                        TabBar {
                            id: tabBar
                            currentIndex: swipeView.currentIndex
                            Layout.fillWidth: true
                            TabButton { text: "7-day view" }
                            TabButton { text: "1-day view" }
                        }
                        SwipeView {
                            id: swipeView
                            Layout.fillWidth: true
                            currentIndex: tabBar.currentIndex
                            clip: true
                            ChartView {
                                id: sevenDaysView
                                implicitHeight: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? width:width/2
                                antialiasing: true
                                legend.labelColor: "white"
                                title: "7-DAY WEATHER FORECAST"; titleColor: "white"
                                backgroundColor: "#252525"
                                animationOptions: ChartView.SeriesAnimations
                                margins.left: 0; margins.right: 0
                                BarCategoryAxis { id: valueAxisX; labelsColor: "white"; titleText: "<font color='white'>Day</font>"; labelsFont.pixelSize: 0.75 * refLabel.font.pixelSize }
                                ValueAxis { id: valueAxisYTemp; labelsColor: "white"; titleText: "<font color='white'>Temperature [\u00B0C]</font>" }
                                ValueAxis { id: valueAxisYRain; labelsColor: "white"; titleText: "<font color='white'>Rainfall [mm]</font>" }
                                BarSeries {
                                    axisX: valueAxisX
                                    axisYRight: valueAxisYRain
                                    BarSet { id: rainfallSet; color: "#03aced"; label: "Rainfall" }
                                }
                                LineSeries { id: minSeries; axisX: valueAxisX; axisY: valueAxisYTemp; name: "Min Temperature" }
                                LineSeries { id: maxSeries; axisX: valueAxisX; axisY: valueAxisYTemp; name: "Max Temperature" }
                            }
                            Item {
                                implicitHeight: dayLayout.implicitHeight
                                ColumnLayout {
                                    id: dayLayout
                                    width: parent.width
                                    ChartView {
                                        id: dayView
                                        Layout.fillWidth: true; Layout.preferredHeight: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? width:width/2
                                        antialiasing: true
                                        legend.labelColor: "white"
                                        title: "TODAY WEATHER FORECAST"; titleColor: "white"
                                        backgroundColor: "#252525"
                                        animationOptions: ChartView.SeriesAnimations
                                        margins.left: 0; margins.right: 0
                                        BarCategoryAxis { id: valueAxisXToday; labelsColor: "white"; titleText: "<font color='white'>Hour</font>"; labelsFont.pixelSize: 0.5 * refLabel.font.pixelSize; titleFont { bold: true } }
                                        ValueAxis { id: valueAxisYLine; labelsColor: "white"; titleText: "<font color='white'>" + lineSeriesVariable.currentText + "</font>" }
                                        ValueAxis { id: valueAxisYBar; labelsColor: "white"; titleText: "<font color='white'>" + barSeriesVariable.currentText + "</font>" }
                                        BarSeries {
                                            axisX: valueAxisXToday
                                            axisYRight: valueAxisYBar
                                            BarSet { id: barSet; color: "#03aced"; label: barSeriesVariable.currentText }
                                        }
                                        LineSeries { id: lineSeries; axisX: valueAxisXToday; axisY: valueAxisYLine; name: lineSeriesVariable.currentText }
                                    }
                                    RowLayout {
                                        Layout.fillWidth: true
                                        property var variablesModel: [ "Temp", "Pressure", "Humidity", "Wind Speed", "Wind Deg", "Clouds", "Pop", "Visibility", "DEW Point" ]
                                        ColumnLayout {
                                            Label { text: "Bar series variable" }
                                            ComboBox { id: barSeriesVariable; Layout.fillWidth: true; model: parent.parent.variablesModel; onCurrentValueChanged: JS.updateSeries(barSet, valueAxisYBar, barSeriesVariable.currentValue, (detailedWeather !== undefined) ? detailedWeather.hourly:undefined, 24) }
                                        }
                                        ColumnLayout {
                                            Label { text: "Line series variable" }
                                            ComboBox { id: lineSeriesVariable; Layout.fillWidth: true; model: parent.parent.variablesModel; currentIndex: parent.parent.variablesModel.length-1; onCurrentValueChanged: JS.updateSeries(lineSeries, valueAxisYLine, lineSeriesVariable.currentValue, (detailedWeather !== undefined) ? detailedWeather.hourly:undefined, 24) }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Component.onCompleted: weatherImage.source = currentWeather ?
                                            ((JS.appid !== "" ? "http://openweathermap.org/img/wn/":"icons/") + currentWeather.weather[0].icon + "@4x.png"):""
}
