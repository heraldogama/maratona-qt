import QtQuick 2.15

Rectangle {
    width: 400
    height: 400
    color: "green"

    Rectangle {
        id: rect
        width: 200
        height: 200
        color: "yellow"
        NumberAnimation on rotation { id: anim; running: false; from: 0; to: 360; duration: 1000 }
        states: State {
            name: "moved"
            PropertyChanges { target: rect; color: "blue" }
            AnchorChanges { target: rect; anchors.right: rect.parent.right }
        }
        transitions: [
            Transition {
                AnchorAnimation { duration: 1000 }
                ColorAnimation { duration: 1000 }
            }
        ]
        MouseArea {
            anchors.fill: parent
            onClicked: { anim.start(); parent.state = (parent.state === "moved") ? "":"moved" }
        }
    }
}
